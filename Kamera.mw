[[Image:Kameraposition_ideal.jpg|thumb|Ideale Kameraposition|200px|right|Ideale Kameraposition]]
[[Image:Testbild.jpg|thumb|Testbild|200px|right|Testbild]]
Das RaumZeitLabor besitzt eine Toshiba Camileo X200 FullHD-Kamera zum Aufzeichnen von Vorträgen. Leider ist der Ton, der aus dieser Kamera rausfällt, nicht all zu gut, deshalb werden Audiospuren seperat aufgezeichnet.

'''Wichtig''': Macht euch mit dieser Anleitung vertraut, bevor ihr die Kamera nutzt!!1!

== Checkliste für Vortrags-Aufzeichnungen ==
Vorbereitung:

# Kamera aus dem Karton im Regal holen und an den Strom anschließen. Tisch-stativ aus dem Regal holen.
# Kamera an der idealen Position aufstellen (siehe Bild).
# Kontrollieren, dass man das vollständige Beamerbild sieht und den Vortragenden.
# Kontrollieren, dass genug Speicher auf der SD-Karte frei ist (die Kamera zeigt auf dem LCD die verbleibende Aufnahmezeit an). Sofern nötig alte Aufnahmen löschen (Aufnahmen müssen nach Benutzung der Kamera direkt kopiert werden, wichtige Daten liegen niemals '''nur''' auf der SD-Karte).
# Audiorekorder aus dem Regal holen und am Tischende aufstellen.
# Kontrollieren, dass genug Speicher auf der SD-Karte frei.
# Einstellungen (Mikrofonwahl, Pegel) überprüfen. Im Zweifelsfall mit angeschlossenen Kopfhörern testen.
# Die Tischlampen vom Tisch nehmen.
# Das Licht beim Beamer ausschalten, bei der Tafel anlassen
# Kamera und Stativ nach der Nutzung wieder an ihren Platz legen

Aufnahme starten:

# Bei der Kamera auf den Aufnahme-Knopf drücken. Ein roter Punkt erscheint im Display.
# Beim Audiorekoreder auf die breite Taste unter dem Display drücken. Auf der Taste leuchtet während der Aufnahme ein roter Punkt.

== Bearbeitung ==

Die Videos der Kamera haben eine Framerate von 29.97 fps, aber eine Field-Anzahl von 59, obwohl sie nicht interlaced sind. Das verwirrt alte Versionen von kdenlive, daher re-encodierte eins früher vor der Weiterverarbeitung:

 ffmpeg -threads 0 -i IMAG0001.MP4 -target ntsc-dvd -sameq -s hd1080 IMAG0001_recoded.mpg

Oder für den ffmpeg-Nachfolger avconv (mit Deinterlacing):

 ffmpeg -threads 0 -i 00000.MTS -target ntsc-dvd -qscale 0 -deinterlace -s hd1080 00000.mpg

Bei den meisten Vorträgen, die den Beamer benutzt haben (abgedunkelter Raum), ist es sinnvoll, das Bild etwas aufzuhellen. In kdenlive kann man dazu den Effekt "Colour correction - Curves" zum Video hinzufügen und einen Punkt oberhalb der Normallinie anklicken (z.B. bei ca. 600x800).

Zur Aufbereitung der Audiospur empfliehlt sich die Datei durch [https://auphonic.com/ Auphonic] laufen zu lassen. Im Zweifelsfall schadet es dabei nicht, alle Filter einzuschalten.

=== Nötige Dateien ===

# [[RZL_Introvideo]] (RZL-Introvideo, Full HD)
# [[Datei:RZL_Vorlage-2012-08-28.tar.bz2]] (Projekt-Datei für kdenlive, Schriftart, CC-BY-Logo, Synchronisations-Sound)
Vorsicht, die Datei ist eine Tarbomb! Am besten für jedes Video so benutzen (wichtig ist, dass man kdenlive im selben Verzeichnis startet):
 cd ~/videos/2012-08-28/
 tar xf ~/Downloads/RZL_Vorlage-2012-08-28.tar.bz2
 kdenlive *.kdenlive

Einmalig muss man die Font installieren:
 tar xf ~/Downloads/RZL_Vorlage-2012-08-28.tar.bz2 -C ~/.fonts miso.otf
 fc-cache -fv

(Die Schriftart stammt von http://martennettelbladt.se/miso/)

[[Category:Hardware]]
[[Category:Procedures]]
[[Category:Vortrag]]
[[Category:Veranstaltung]]
